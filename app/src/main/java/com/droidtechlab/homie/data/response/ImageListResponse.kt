package com.droidtechlab.homie.data.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ImageListResponse(
    @SerializedName("total") var total: Int = -1,
    @SerializedName("totalHits") var totalHits: Int = -1,
    @SerializedName("hits") var imgList: List<ImageObject>? = null,
): Parcelable

@Parcelize
data class ImageObject(
    @SerializedName("id") var id: Int = -1,
    @SerializedName("pageUrl") var pageUrl: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("tags") var tags: String? = null,
    @SerializedName("previewURL") var previewURL: String? = null,
    @SerializedName("webformatURL") var webformatURL: String? = null,
    @SerializedName("previewWidth") var previewWidth: Int = -1,
    @SerializedName("previewHeight") var previewHeight: Int = -1,
    @SerializedName("webformatWidth") var webformatWidth: Int = -1,
    @SerializedName("webformatHeight") var webformatHeight: Int = -1,
    @SerializedName("largeImageURL") var largeImageURL: String? = null,
    @SerializedName("fullHDURL") var fullHDURL: String? = null,
    @SerializedName("imageURL") var imageURL: String? = null,
    @SerializedName("user") var user: String? = null,
    @SerializedName("userImageURL") var userImageURL: String? = null,
    @SerializedName("views") var views: Int = -1,
    @SerializedName("downloads") var downloads: Int = -1,
    @SerializedName("likes") var likes: Int = -1,
    @SerializedName("comments") var comments: Int = -1,
): Parcelable
