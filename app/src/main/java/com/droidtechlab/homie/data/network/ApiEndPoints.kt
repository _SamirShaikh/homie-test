package com.droidtechlab.homie.data.network

import com.droidtechlab.homie.data.response.ImageListResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiEndPoints {

    @GET("/api/?key=23037544-598c2676b41b85e763ff67831")
    fun getListOfImage(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
        @Query("q") query: String
    ): Observable<ImageListResponse>
}