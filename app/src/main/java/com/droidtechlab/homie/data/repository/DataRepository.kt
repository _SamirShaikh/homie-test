package com.droidtechlab.homie.data.repository

import androidx.lifecycle.MutableLiveData
import com.droidtechlab.homie.data.network.*
import com.droidtechlab.homie.data.response.ImageListResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class DataRepository {


    private val retrofit = ApiClient.apiClient?.create(ApiEndPoints::class.java)!!
    private val compositeDisposable = CompositeDisposable()

    fun getListOfImage(
        page: Int,
        perPage: Int,
        query: String
    ): MutableLiveData<Results<ImageListResponse>> {
        val liveData = MutableLiveData<Results<ImageListResponse>>()

        compositeDisposable.add(
            retrofit.getListOfImage(page, perPage, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.imgList?.size!! > 0) {
                        liveData.postValue(Success(it))
                    } else {
                        liveData.postValue((Error(it)))
                    }
                }, { t ->
                    liveData.postValue(Failure(t ?: Throwable("null exception")))
                })
        )
        return liveData
    }

    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}