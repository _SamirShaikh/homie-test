package com.droidtechlab.homie.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.droidtechlab.homie.R
import com.droidtechlab.homie.databinding.GenericRecyclerFragmentBinding
import com.droidtechlab.homie.ui.adapter.GenericRecyclerAdapter
import com.droidtechlab.homie.ui.viewholders.AbstractViewHolder
import com.droidtechlab.homie.ui.viewholders.ProgressViewHolder
import com.droidtechlab.homie.utils.VerticalSpaceItemDecoration

abstract class GenericRecyclerFragment : Fragment() {

    lateinit var mBinding: GenericRecyclerFragmentBinding
    lateinit var linearLayoutManager: LinearLayoutManager
    var page = 1
    var isLoading = false
    var observableList = ObservableArrayList<AbstractViewHolder>()
    var adapter = GenericRecyclerAdapter(observableList)
    var  progressViewHolder = ProgressViewHolder()
    var mQuery: String = ""


    protected val actionBar: ActionBar?
        get() = appCompatActivity?.supportActionBar

    private val appCompatActivity: AppCompatActivity?
        get() = activity as? AppCompatActivity?

    var stubView: View? = null

    protected fun setSupportActionBar(toolbar: Toolbar) {
        appCompatActivity?.setSupportActionBar(toolbar)
    }

    private fun increasePageCount() {
        page += 1
    }

    private fun decreasePageCount() {
        page -= 1
    }

    fun onRequestStarted() {
        isLoading = true
        addItem(progressViewHolder)
    }

    fun onRequestCompleted() {
        increasePageCount()
        removeItem(progressViewHolder)
        isLoading = false
    }

    fun onApiError() {
        decreasePageCount()
        removeItem(ProgressViewHolder())
        isLoading = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.generic_recycler_fragment, container, false)
        linearLayoutManager = LinearLayoutManager(requireContext())
        mBinding.recyclerView.layoutManager = linearLayoutManager
        mBinding.recyclerView.addItemDecoration(
            VerticalSpaceItemDecoration(
                resources.getDimension(R.dimen.vertical_margin_half).toInt()
            )
        )
        mBinding.recyclerView.adapter = adapter
        return mBinding.root
    }

    fun addItem(model: AbstractViewHolder) {
        observableList.add(model)
        adapter.notifyItemInserted(observableList.size + 1)
    }

     fun removeItem(model: AbstractViewHolder) {
        observableList.remove(model)
        adapter.notifyDataSetChanged()
    }

    fun removeAllItem() {
        observableList.clear()
        adapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && dy > 0 && lastVisibleItemPosition != -1 && lastVisibleItemPosition == linearLayoutManager.itemCount - 1 && !mQuery.isNullOrEmpty()) {
                    loadData(mQuery)
                }
            }
        })
    }

    fun setStubView(layout: Int) {
        mBinding.stub.viewStub?.apply {
            if (!mBinding.stub.isInflated) {
                layoutResource = layout
                stubView = mBinding.stub.viewStub?.inflate()
            }
        }
    }

    fun updateList(listOfViews: MutableList<AbstractViewHolder>) {
        observableList.addAll(listOfViews)
        val adapter = GenericRecyclerAdapter(observableList)
        mBinding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    abstract fun loadData(query: String)

    /**
     *  Check's if the fragment is detached or is being removed from it's parent
     *  Helps to consume code flow so that UI operations are not performed while fragment is removing or already detached.
     */
    fun consumeCallback(): Boolean {
        return isDetached || isRemoving || context == null
    }

}