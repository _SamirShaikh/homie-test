package com.droidtechlab.homie.ui.viewholders

import android.util.Log
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.RequestManager
import com.droidtechlab.homie.R
import com.droidtechlab.homie.data.response.ImageObject
import com.droidtechlab.homie.ui.OnImageClickListener

class RowImageViewHolder(var imageObject: ImageObject, var reqManager: RequestManager, var listener: OnImageClickListener) :
    AbstractViewHolder() {

    override fun getLayoutIdentifier(): Int {
        return R.layout.row_image_item
    }

    fun openImageDetail(v: View) {
        listener.openImageDetail(imageObject)
    }

    companion object {

        @JvmStatic
        @BindingAdapter("loadImage")
        fun loadImage(imgView: AppCompatImageView, model: RowImageViewHolder) {
            model.reqManager.load(model.imageObject.webformatURL)
                .placeholder(R.drawable.placeholder).into(imgView)
        }

        @JvmStatic
        @BindingAdapter("loadUserImage")
        fun loadUserImage(imgView: AppCompatImageView, model: RowImageViewHolder) {
            model.reqManager.load(model.imageObject.userImageURL)
                .circleCrop()
                .placeholder(R.drawable.placeholder).into(imgView)
        }
    }
}