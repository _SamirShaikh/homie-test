package com.droidtechlab.homie.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.droidtechlab.homie.R
import com.droidtechlab.homie.data.network.Error
import com.droidtechlab.homie.data.network.Failure
import com.droidtechlab.homie.data.network.Success
import com.droidtechlab.homie.data.response.ImageObject
import com.droidtechlab.homie.ui.viewholders.RowImageViewHolder
import com.droidtechlab.homie.ui.viewmodels.MainViewModel
import com.droidtechlab.homie.utils.NetworkUtils
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ImageListFragment : GenericRecyclerFragment(), OnImageClickListener {

    private lateinit var mainViewModel: MainViewModel
    private var searchTxt: AppCompatEditText? = null
    private val compositeDisposable = CompositeDisposable()
    lateinit var navController: NavController


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStubView(R.layout.row_search_lyt)
        navController = Navigation.findNavController(view)
        searchTxt = stubView?.findViewById<AppCompatEditText>(R.id.search)
        searchTxtChangeListener()
    }

    private fun searchTxtChangeListener() {

        compositeDisposable.add(
            searchTxt?.textChanges()?.map {
                mQuery = it.toString()
            }?.debounce(300, TimeUnit.MILLISECONDS)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    if(!NetworkUtils.isNetworkAvailable(requireContext())) {
                        Toast.makeText(requireContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show()
                    } else {
                        removeAllItem()
                        loadData(mQuery)
                    }
                }!!
        )
    }


    override fun loadData(query: String) {

        if (consumeCallback()) return

        onRequestStarted()
        mainViewModel.getListOfImage(page, 10, query)
            .observe(viewLifecycleOwner, Observer { results ->
                when (results) {
                    is Success -> {
                        results.data.imgList?.forEach {
                            addItem(
                                RowImageViewHolder(
                                    it,
                                    Glide.with(this@ImageListFragment),
                                    this@ImageListFragment
                                )
                            )
                        }
                        onRequestCompleted()
                    }
                    is Error -> {
                        Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                        onApiError()
                    }
                    is Failure -> {
                        Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                        onApiError()
                    }
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun openImageDetail(imgObject: ImageObject) {
        navController.navigate(
            R.id.action_imageListFragment_to_imageDetailsFragment,
            Bundle().apply {
                putParcelable(IMG_OBJECT, imgObject)
            })
    }

    companion object {
        const val IMG_OBJECT = "img_object"
    }

}


interface OnImageClickListener {
    fun openImageDetail(imgObject: ImageObject)
}