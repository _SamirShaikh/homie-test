package com.droidtechlab.homie.ui.viewholders

import com.droidtechlab.homie.R

class ProgressViewHolder:  AbstractViewHolder() {

    override fun getLayoutIdentifier(): Int {
        return R.layout.row_progress_view
    }
}