package com.droidtechlab.homie.ui.viewholders

abstract class AbstractViewHolder {
    abstract fun getLayoutIdentifier(): Int
}