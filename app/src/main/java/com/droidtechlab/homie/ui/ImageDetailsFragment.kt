package com.droidtechlab.homie.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.droidtechlab.homie.R
import com.droidtechlab.homie.data.response.ImageObject
import com.droidtechlab.homie.databinding.FragmentImageDetailsBinding


class ImageDetailsFragment : Fragment() {


    private var imgObject: ImageObject? = null
    private lateinit var mBinding: FragmentImageDetailsBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.apply {
            imgObject = getParcelable<ImageObject>(IMG_OBJECT)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_image_details, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this@ImageDetailsFragment).load(imgObject?.largeImageURL)
            .placeholder(R.drawable.placeholder).into(mBinding.imageView)
    }

    companion object {
        const val IMG_OBJECT = "img_object"
    }


}