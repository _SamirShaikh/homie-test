package com.droidtechlab.homie.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.droidtechlab.homie.data.network.Results
import com.droidtechlab.homie.data.repository.DataRepository
import com.droidtechlab.homie.data.response.ImageListResponse

class MainViewModel : ViewModel() {

    private val repo = DataRepository()

    fun getListOfImage(page: Int, perPage: Int,  query: String): MutableLiveData<Results<ImageListResponse>> {
        return repo.getListOfImage(page, perPage, query)
    }

    override fun onCleared() {
        super.onCleared()
        repo.dispose()
    }
}